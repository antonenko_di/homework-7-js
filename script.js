/*
Написати функцію filterBy(), яка прийматиме 2 аргументи.
Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.
Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].*/


function filterBy(array, string) {
    let result = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== string) {
            result.push(array[i]);
        }
    }
    return result;
}

let newArray = ["1", 2, "hello world", true, null, "name", 45];
let filterArray = filterBy(newArray, "string");
console.log(filterArray);



